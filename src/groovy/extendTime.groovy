#!/usr/bin/env groovy
import java.util.concurrent.*


/*
Sunday,25,12,2011,Christmas Day
Sunday,1,1,2012,New Years Day
Sunday,5,2,2012,The day after the prophets birthday
Monday,13,2,2012,Post African Cup of Nations Recovery
Monday,9,4,2012,Easter Monday
*/
def pool = Executors.newFixedThreadPool(10)
def defer = { c -> pool.submit(c as Callable)}

def extendTime = { i ->
	def holidays = ["2011-12-25","2012-01-01","2012-02-05","2012-02-13","2012-04-09"]
	def calendar = Calendar.instance

	File data 	= new File("../data/SET1TSV/SET1TSV_${i}.TSV");
	File dataExt 	= new File("../data/SET1TSV/SET1TSV_${i}_EXT.CSV");
	println "reading file ${data}"

	dataExt.withWriter { out ->

	data.eachLine { line ->
		columns = line.split()
		def date 		= columns[0]
		def time 		= columns[1]
		def ant1 		= columns[2]
		def ant2 		= columns[3]
		def calls 		= columns[4]
		def duration 	= columns[5]

		calendar.time	= Date.parse("yyyy-MM-dd HH:mm:ss", "${date} ${time}")
			
		def weekday		= calendar.time.format("EEEE")

		def hourOfDay	= calendar.get(Calendar.HOUR_OF_DAY)

		def interval	= ((hourOfDay < 6) && (hourOfDay >= 0))? "Evening" : ((hourOfDay < 12) && (hourOfDay >= 6))? "Morning" : ((hourOfDay < 18) && (hourOfDay >= 12))? "Afternoon" : "Night";

		def isHoliday	= holidays.contains(date) || weekday == "Saturday" || weekday == "Sunday"

		def writeLine	= "${date},${time},${weekday},${isHoliday},${interval},${ant1},${ant2},${calls},${duration}"

		//println "[${i}]${writeLine}"
		out.writeLine("${writeLine}")
	}

	}
}


(0..9 ).each{n -> 
	println "Executing ${n}"
	//extendTime(n)

	def th = Thread.start {extendTime(n)}
}
//pool.shutdown()
