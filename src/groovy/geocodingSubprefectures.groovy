#!/usr/bin/env groovy

@Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7' )
import groovyx.net.http.*
import static groovyx.net.http.ContentType.*
import static groovyx.net.http.Method.*

File antPos 	= new File("../data/SUBPREF_POS_LONLAT.TSV");
File antPosExt 	= new File("../data/SUBPREF_POS_LONLAT_EXT.CSV");
println "reading file ${antPos}"

def http = new HTTPBuilder()


antPosExt.withWriter { out ->

	antPos.eachLine { line ->
		columns = line.split()
		def pos = columns[0]
		def lng = columns[1]
		def lat = columns[2]
			
		println "${pos} -> Lat: ${lat}, Lng: ${lng}"
			
		def geoApi 		= new RESTClient("https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&sensor=false&key=<apiKey>")
		def resp 		= geoApi.get( contentType : JSON)
		def addresses 	= resp.data.results[0].address_components
		def route,sublocality,locality,adminArea2,adminArea1,country = "empty"
		try{
			addresses.each { address ->
				if ("route" in address.types){
					route = address.short_name
				}
				else if ("sublocality" in address.types){
					sublocality = address.short_name
				}
				else if ("sublocality" in address.types){
					sublocality = address.short_name
				}
				else if ("locality" in address.types){
					locality = address.short_name
				}
				else if ("administrative_area_level_2" in address.types){
					adminArea2 = address.short_name
				}
				else if ("administrative_area_level_1" in address.types){
					adminArea1 = address.short_name
				}
				else if ("country" in address.types){
					country = address.short_name
				}
			}

		} catch (Exception e){
			println e
		}

		out.writeLine("${pos},${lng},${lat},${route},${sublocality},${locality},${adminArea2},${adminArea1},${country}")
		sleep 10
	}

}